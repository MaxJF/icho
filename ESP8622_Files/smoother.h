#ifndef SMOOTHER
#define SMOOTHER

class smoother
{
private:
  float smoothedValue;
  float smoothFactor;

public:
  smoother();
  ~smoother();
  
  void setSmoothFactor(float factor);
  int getSmoothed(int newValue);
};

#endif