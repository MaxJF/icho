#ifndef BASELINE
#define BASELINE

#include <Arduino.h>

class baseline
{
public:
  baseline();
  ~baseline();
  
  void setStayOn(boolean stayOnActive);
  
  void update(float value, long currentTime);
  float getValue();
  
private:
  
  boolean stayOn;
  
  float maxDiff;
  float baseLineValue;
  float currentValue;
  
};

#endif 
