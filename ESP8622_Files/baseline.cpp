#include "baseline.h"

baseline::baseline()
{
  baseLineValue = 0;
  
  stayOn = false;
}

baseline::~baseline()
{
  
}
  
void baseline::update(float value, long currentTime)
{
  currentValue = value;
   
  if (stayOn)
  {
    if (currentValue<baseLineValue)
      baseLineValue = 0.8*baseLineValue + 0.2*currentValue;
  }
  else
    baseLineValue = 0.99*baseLineValue + 0.01*currentValue;
    
  float diff = abs(baseLineValue - currentValue);

  if (diff>maxDiff)
      maxDiff = diff;
 
  maxDiff = 0.999*maxDiff + 0.001*diff;
}

float baseline::getValue()
{
    float diff = abs(baseLineValue - currentValue);
    return diff/maxDiff;
}

void baseline::setStayOn(boolean stayOnActive)
{
  stayOn = stayOnActive;
}
