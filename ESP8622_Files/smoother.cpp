#include "smoother.h"

smoother::smoother()
{
	smoothedValue = 0.0f;
}
  
smoother::~smoother()
{

}
  
void smoother::setSmoothFactor(float factor)
{
	smoothFactor = factor;
}
  
int smoother::getSmoothed(int newValue)
{
	smoothedValue = (1.0f-smoothFactor)*(float)newValue + smoothFactor*smoothedValue;
	return (int)smoothedValue;
}


