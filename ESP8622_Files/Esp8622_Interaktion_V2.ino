bool DeBug = true;
bool pres = false;


#include <ESP8266WiFi.h>

//anmelden im WLAN des Servers
const char* ssid     = "AndroidAP";
const char* password = "thfh4183";
WiFiClient client;
const char* host= "192.168.43.15";
const char* streamId   = "....................";
const char* privateKey = "....................";
int buttonState=0;


// Empfangene Werte vom Sever / Wertebereich von 0 - 255
  
  int icap;       // Wert des Kapazitiven Sensors
  int ipres;      // Druck
  int ivibro;     // Vibration
  int iR;         // R  NeoPixel
  int iG;         // G  NeoPixel
  int iB;         // B  NeoPixel
  int ibright;    // Helligkeit der Neopixel



 //PRESSURE

 int iwerte[10] = {750,750,750,750,750,750,750,750,750,750};
 int inull = 750;
 int idif =0;



//VIBRATION

long intervalTime = 200;
long lastTime=0;
long stateTime = 0;





//GYROSKOP

#include "I2Cdev.h"
#include "MPU6050.h"

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

MPU6050 accelgyro;

int16_t ax, ay, az;
int16_t gx, gy, gz;



// NeoPIXEL
//
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif
Adafruit_NeoPixel strip = Adafruit_NeoPixel(6, 0, NEO_GRB + NEO_KHZ800);

//CapacitiveSensor capSensor(13, 12);



 void setup() 
{ 
  Serial.begin(9600);  
  //VIBRO
  pinMode(2,OUTPUT);

   // NEOPIXEL
  strip.begin();               // Initialize all pixels to 'off'
  strip.show();
  if (DeBug)
    {
    Serial.println("Strip started");
    }

  if (DeBug){
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  }
  // We start by connecting to a WiFi network
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(500);
    if (DeBug) {
    Serial.print(".");
    }
  }
  if (DeBug) {
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  }
  const int httpPort = 8000;
  client.setTimeout(20);
  
    while (!client.connect(host, httpPort)) 
    {
      if (DeBug) {
    Serial.println("connection failed");
      }
    }
    if (client.connect(host,httpPort))
    {
      if (DeBug) {
      Serial.println("CONNECTED");
      }
     delay (500);
    }

// GYROSKOP

    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin(4,5);
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif
    // initialize device
        if (DeBug) {
    Serial.println("Initializing I2C devices...");
    }
    accelgyro.initialize();
//    while(accelgyro.testConnection()==0){
//    accelgyro.initialize();
//    if (DeBug) {
//    Serial.println("connecting again...");
//    }
//    }
    if (DeBug) {
    Serial.println("Gryo Initialisiert");
    // verify connection
    Serial.println("Testing device connections...");
    Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");
    Serial.println((String)accelgyro.testConnection());
    }
}

void loop() {

   //PRESSURE

   for (int i=9;i>=1;i--)
  {
    iwerte[i]=iwerte[i-1];  
  }
  iwerte[0]=analogRead(A0);
  idif=iwerte[0]-iwerte[5];
  if((idif>-2)&&(!pres))
  {
    inull=iwerte[0];
    ipres=0;
  }
  if((abs(idif)>=2)&&(!pres))
    pres=true;
  if((idif>50)&&pres)
  {
    pres=false;
  }
  if(pres)
  {
    ipres=iwerte[0]-inull;
    ipres=abs(ipres);
    ipres=ipres*5;
    if(ipres>255)
      ipres=255;
  }
    if (DeBug) {
      Serial.println("analogRead: "+(String)analogRead(A0)+"\t"+(String)ipres);
    }
// GYROSKOP

    // read raw accel/gyro measurements from device
    
  accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);



  //map Gyro Data
  int iax = map(ax, -16000, 16000, 0, 255);
  int iay = map(ay, -16000, 16000, 0, 255);
  int iaz = map(az, -16000, 16000, 0, 255);
  int igx = map(gx, -16000, 16000, 0, 255);
  int igy = map(gy, -16000, 16000, 0, 255);
  int igz = map(gz, -16000, 16000, 0, 255);
  
  // Use WiFiClient class to create TCP connections
  // This will send the request to the server
   // Werte an HOST schicken als String
  client.print((String)icap + "," + (String)iax + "," + (String)iay + "," + (String)iaz + "," + (String)igx + "," + (String)igy + "," +(String)igz + "," + (String)ipres); 
  //Serial.print((String)iax + "," + (String)iay + "," + (String)iaz); 
 
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    
    ivibro = client.parseInt();
    iR = client.parseInt();
    iG = client.parseInt();
    iB = client.parseInt();
    ibright = client.parseInt();
    client.flush();
    //Serial.print((String)ivibro + "," +(String)iR + "," + (String)iG + "," +(String)iB +"," + (String)ibright);
  }
  ibright = map(ibright,0,255,0,200)+55;
  //VIBRO
  if(DeBug){
  Serial.println((String)ivibro);
  }
  float myPWM = 0.001f*(float)map(ivibro, 0, 255, 0, 1000);
  long currentTime = millis();
  long timeDifference = currentTime-lastTime; 
  stateTime+=timeDifference;
  if (stateTime>intervalTime)
    stateTime = 0;
   
  long transitionTime = constrain((myPWM+0.1)*intervalTime, 0, intervalTime);
  if (myPWM<0.1f)
    digitalWrite(2, LOW); 
  else
  {
    if (stateTime>=transitionTime)
    {
      digitalWrite(2, LOW);    
    }
    else
    {
      digitalWrite(2, HIGH);  
    }
  }
  
  lastTime = currentTime;
 

  //NEOPIXEL

   for (int i=0;i<=5;i++)
  {
  strip.setPixelColor(i, (iR * ibright / 255), (iG * ibright / 255), (iB * ibright / 255));  
  }

  strip.show();
  //Serial.println("icap:" + (String)icap + " prsr:"+  (String)ipres +" R:"+ (String)iR + " G:"+ (String)iG + " B:"+ (String)iB +" bright:"+ (String)ibright+" ivibro:"+ (String)ivibro);
    if (DeBug) {
      Serial.println("icap:" + (String)icap + " prsr:"+  (String)ipres +" R:"+ (String)iR + " G:"+ (String)iG + " B:"+ (String)iB +" bright:"+ (String)ibright+" ivibro:"+ (String)ivibro);
    }
}

