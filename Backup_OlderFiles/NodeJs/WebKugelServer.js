/*---- DEBUGGING VARIABLES ----*/
var debugInfoFromSpheres=false;
var debugOutgoingInfo=false;
/*---- DEBUGGING VARIABLES END ----*/

var http = require('http');
var fs = require('fs');
var net = require('net');
var server = net.createServer();  
var connect = require('connect');
var sendStrings=[];
var connections=[];
var clients=-1;
var GyroOutputK1="";
var GyroOutputK2="";
var resGlob;


/*---- WEBSERVER START----*/
var express = require('express'),
app = express();
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded());

app.get('/', function (req, res) {
  res.render(__dirname + '/index.html',{k1Gyro:GyroOutputK1,k2Gyro:GyroOutputK2});

});

app.post('/getJson', function (req, res) {
  //res.json({k1Gyro: 'NewTitle'});
  //res.send({k1Gyro:GyroOutputK1,k2Gyro:GyroOutputK2});
  res.render(__dirname + '/index.html',{k1Gyro:GyroOutputK1,k2Gyro:GyroOutputK2});
  sendDataToSphere("1",req.body.selectpicker);
});
app.post('/getJson2', function (req, res) {
  res.render(__dirname + '/index.html',{k1Gyro:GyroOutputK1,k2Gyro:GyroOutputK2});

  sendDataToSphere("2",req.body.selectpicker2);
});

app.listen(8080);

function sendDataToSphere(kugel,color){

    if(sendStrings
[kugel-1]){
      switch(color){
        case 'R': 
        sendStrings
[kugel-1]=("0,254,0,0,0");
        console.log("R");
        break;
        case 'G':
        sendStrings
[kugel-1]=("0,0,254,0,0");
console.log("G");
        break;
        case 'B':
        sendStrings
[kugel-1]=("0,0,0,254,0");
console.log("B");
        break;
      }
console.log("setting Sending Array for sphere "+kugel+" to "+sendStrings[kugel-1]);
console.log("now there are "+sendStrings.length +"Strings for "+ connections.length+"spheres registered");
    }


}


/*---- WEBSERVER END ----*/


/*---- KUGEL HANDLING START ----*/
server.on('connection', handleConnection);

server.listen(8000, function() {  
  console.log('server listening to %j', server.address());
});

function handleConnection(conn) {  
  var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
  connections.push(conn);
  sendStrings.push("0,0,0,0,0");
  conn.sphereNumber
=connections.length;
  console.log('new client connection from %s', remoteAddress);
  
  var thisclient=clients;

  conn.on('data', onConnData);
  conn.once('close', onConnClose);
  conn.on('error', onConnError);

  function onConnData(d) {
    if(conn.sphereNumber
==1){
      GyroOutputK1=d;
    
    }
    if(conn.sphereNumber
==2){
      
      GyroOutputK2=d;
    }
    if(debugInfoFromSpheres){
    console.log('connection data from %s: %j', remoteAddress, d);
    }
    
    if(sendStrings
[conn.sphereNumber
-1]){
    conn.write(sendStrings
[conn.sphereNumber
-1]);
    if(debugOutgoingInfo){
      console.log("Data send to Sphere #"+conn.sphereNumber
+": "+sendStrings
[conn.sphereNumber
-1])
    }

  }
    else{
    conn.write("0,0,0,0,0");
if(debugOutgoingInfo){
      console.log("Data send to Sphere #"+conn.sphereNumber
+": 0,0,0,0,0")
    }
  }
    
  }

  function onConnClose() {

    console.log('connection from %s closed', remoteAddress);
        console.log("DELETING SPHERE #"+conn.sphereNumber
);
    if(conn.sphereNumber
<connections.length){
    for (var i = 0 ;i <connections.length; i++) {
      connections[i].sphereNumber--;
    };}
    connections.splice(conn.sphereNumber
-1,1);
    sendStrings.splice(conn.sphereNumber
-1,1);

  }

  function onConnError(err) {
    console.log('Connection %s error: %s', remoteAddress, err.message);
  }
}
/*---- KUGEL HANDLING END ----*/