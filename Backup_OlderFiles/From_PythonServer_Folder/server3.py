from socket import *
import thread




BUFF = 1024
HOST = '172.20.10.3'# must be input parameter @TODO
PORT = 8000 # must be input parameter @TODO

global grx          #Gyro x Axes
global gry          #Gyro y Axes
global grz          #Gyro z Axes
global prsr         #Pressure
#global dstn        #Capativity




def response(grx , gry, grz, prsr):
    return  '{0},{1},{2},{3}'.format(grx, gry, gry, prsr)


def handler(clientsock,addr):
    while 1:

        try:
            data = clientsock.recv(BUFF)
            if data == '':break         #In case the String is empty           
            b = data.split(",")         #Split received string in to variables
            c = [int(e) for e in b]
            grx,gry,grz,prsr = c
            print 'data:' + repr(data)
            if not data: break
            #colorChange (grx, gry, grz)     #LED lights Color
            #sendPressure (prsr)            #calculates if prsr high enough for vibrating impuls
            #capacityActivation             #Through going near the sphere, multiplayer is activated
            
                        
            clientsock.send(response(grx, gry, grz, prsr))
            print 'sent:' + repr(response(grx, gry, grz, prsr))
            #clientsock.close() # - reports [Errno 9] Bad file descriptor as it looks like that socket is trying to send data when it is already closed


        except ValueError :
                print ('Ignoring malformed line : {}"'.format(data))
        


if __name__=='__main__':
    ADDR = (HOST, PORT)
    serversock = socket(AF_INET, SOCK_STREAM)
    serversock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    serversock.bind(ADDR)
    serversock.listen(5)
    while 1:
        print 'waiting for connection...'
        clientsock, addr = serversock.accept()
        print '...connected from:', addr
        thread.start_new_thread(handler, (clientsock, addr))
