from socket import *
from threading import Thread
import  socket, threading
from Queue import *
import time


BUFF = 1024
HOST = '172.20.10.3'# must be input parameter @TODO
PORT = 8000 # must be input parameter @TODO


#global grx          #Gyro x Axes
#global gry          #Gyro y Axes
#global grz          #Gyro z Axes
#global prsr         #Pressure
#global dstn        #Capativity
global savedString
global exchangeString
exchangeString = '0,0,0,0'


def data_exchange(current_data):
    
    global savedString
    global exchangeString
    savedString = current_data
    current_data = exchangeString
    exchangeString = savedString

    return current_data



def response(grx , gry, grz, prsr):
    return  '{0},{1},{2},{3}'.format(grx, gry, gry, prsr)



class ClientThread(threading.Thread):

    def __init__(self, ip, port, socket, q):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.socket = socket
        self.q = q
        print "[+] New thread started for " +ip +":" + str(port)

    def run(self):
  
   
        while 1:
              
              try:
                  
                  data = self.socket.recv(BUFF)
                  if data == '': break
                  #print 'Puffer'
                  #if data != '':
                  #print 'In IF'
                  
                  data_exchange(self.q.get(data))
                  self.q.put(data)
                  b = data.split(",")         #Split received string in to variables
                  c = [int(e) for e in b]
                  grx,gry,grz,prsr = c
                  print 'data:' + repr(data)
                  #if not data: continue
                  
                  
                  #colorChange (grx, gry, grz)     #LED lights Color
                  #sendPressure (prsr)            #calculates if prsr high enough for vibrating impuls
                  #capacityActivation             #Through going near the sphere, multiplayer is activated
                  self.socket.send(response(grx, gry, grz, prsr))
                  print 'sent:' + repr(response(grx, gry, grz, prsr))

                  
              except ValueError:
                print ('Ignoring malformed line : {}"'.format(data))


        print "Client disconnected"


tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((HOST,PORT))
threads = []




while 1:
    q = Queue()
    tcpsock.listen(5)
    print "\nListening for incoming connections..."
    (clientsock, (ip, port)) = tcpsock.accept()
    newthread = ClientThread(ip, port, clientsock, q)
    newthread.start()
    threads.append(newthread)
    

for t in threads:
    t.join()
    
    



