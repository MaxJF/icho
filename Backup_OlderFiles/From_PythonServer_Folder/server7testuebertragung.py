from socket import *
from threading import Thread
import  socket, threading



BUFF = 1024
HOST = '192.168.43.15'# must be input parameter @TODO
PORT = 8000 # must be input parameter @TODO



global savedString
global exchangeString
exchangeString = '0,0,0,0'
global data1
global data2
data1='0,0,0,0'
data2='0,0,0,0'


def data_exchange(current_data):
    
    global savedString
    global exchangeString
    savedString = current_data
    current_data = exchangeString
    exchangeString = savedString

    return current_data



def response(grx , gry, grz, prsr):
    return  '{0},{1},{2},{3}'.format(grx, gry, gry, prsr)



class ClientThread(threading.Thread):

    def __init__(self, ip, port, socket):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.socket = socket
        print "[+] New thread started for " +ip +":" + str(port)

    def run(self):
  
   
        while 1:
              
              try:
                  global data1
                  global data2
                  if self.ip == '192.168.43.25':
                      data1 = self.socket.recv(BUFF)
                      if data1 == '': break    


                      b = data2.split(",")         #Split received string in to variables
                      c = [int(e) for e in b]
                      capa,prsr,ax,ay,az,grx,gry,grz, = c
                      print 'data1:' + repr(data2) + ' data2:' + repr(data1)
                      #if not data: continue
                      
                      
                      #colorChange (grx, gry, grz)     #LED lights Color
                      #sendPressure (prsr)            #calculates if prsr high enough for vibrating impuls
                      #capacityActivation             #Through going near the sphere, multiplayer is activated
                      #self.socket.send(response(grx, gry, grz, prsr))
                      #print 'sent:' + repr(response(grx, gry, grz, prsr))
                      self.socket.send("100,200,300,400,500,100,")
                      print 'sent:' + repr("100,200,300,400,500,600,")

                  if self.ip == '192.168.43.26':
                      data2 = self.socket.recv(BUFF)
                      if data2 == '': break    


                      b = data1.split(",")         #Split received string in to variables
                      c = [int(e) for e in b]
                      grx,gry,grz,prsr = c
                      print  'data1:' + repr(data2) + ' data2:' + repr(data1)
                      #if not data: continue
                      
                      
                      #colorChange (grx, gry, grz)     #LED lights Color
                      #sendPressure (prsr)            #calculates if prsr high enough for vibrating impuls
                      #capacityActivation             #Through going near the sphere, multiplayer is activated
                      #self.socket.send(response(grx, gry, grz, prsr))
                      #print 'sent:' + repr(response(grx, gry, grz, prsr))
                      self.socket.send("100,200,90,150,200,176,")
                      print 'sent:' + repr("100,200,90,150,200,176,")
                  
              except ValueError:
                print ('Ignoring malformed line : {}"'.format(data1))


        print "Client disconnected"


tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((HOST,PORT))
threads = []




while 1:

    tcpsock.listen(5)
    print "\nListening for incoming connections..."
    (clientsock, (ip, port)) = tcpsock.accept()
    newthread = ClientThread(ip, port, clientsock)
    newthread.start()
    threads.append(newthread)
    

for t in threads:
    t.join()
    
    



