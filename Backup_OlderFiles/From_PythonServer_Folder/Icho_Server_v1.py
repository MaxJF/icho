from socket import *
from threading import Thread
import  socket, threading



BUFF = 1024
HOST = '192.168.43.15'# must be input parameter @TODO
PORT = 8000 # must be input parameter @TODO



global savedString
global exchangeString
exchangeString = '0,0,0,0'
global data1
global data2
global dataSingle
data1='0,0,0,0,0,0,0,0'
data2='0,0,0,0,0,0,0,0'
dataSingle='0,0,0,0,0,0,0,0' 
global clients
clients = 0

def data_exchange(current_data):
    
    global savedString
    global exchangeString
    savedString = current_data
    current_data = exchangeString
    exchangeString = savedString

    return current_data



def response(ipres,iax,iay,iaz,icap):
    return  '{0},{1},{2},{3},{4}'.format(ipres,iax,iay,iaz,icap)



class ClientThread(threading.Thread):

    def __init__(self, ip, port, socket):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.socket = socket
        print "[+] New thread started for " +ip +":" + str(port)

    def run(self):



       while (1):
             try:
                  global data1
                  global data2
                  #IP Adresse der 1.Kugel
                  if self.ip == '192.168.43.173':
                      data2 = self.socket.recv(BUFF)
                      if data2 == '': break
                      b = data1.split(",")         #Split received string in to variables
                      c = [int(e) for e in b]
                      icap, iax, iay, iaz, igx, igy, igz, ipres = c
                      #print 'data1:' + repr(data1) + ' data2:' + repr(data2)
                      if not data1: break
                      self.socket.send(response(ipres,iax,iay,iaz,ipres))
                      #print 'KUGEL 1: ' + str(ipres)
                      print 'kugel1 sent:' + repr(response(ipres,iax,iay,iaz,icap))


                      
                  #IP Adresse der 2.Kugel
                  if self.ip == '192.168.43.178':
                      data1 = self.socket.recv(BUFF)
                      if data1 == '': break
                      b = data2.split(",")         #Split received string in to variables
                      c = [int(e) for e in b]
                      icap, iax, iay, iaz, igx, igy, igz, ipres = c
                      #print  'data1:' + repr(data1) + ' data2:' + repr(data2)
                      #print 'Kugel2: ' + str(ipres)
                      if not data2: break
                      self.socket.send(response(ipres,iax,iay,iaz,ipres))
                      print 'kugel 2 sent:' + repr(response(ipres,iax,iay,iaz,icap ))

                  
             except ValueError:
                print ('Ignoring malformed line : {}'.format(data1))


    print "Client disconnected"
    


tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((HOST,PORT))
threads = []




while 1:

    tcpsock.listen(5)
    print "\nListening for incoming connections..."
    (clientsock, (ip, port)) = tcpsock.accept()
    newthread = ClientThread(ip, port, clientsock)
    newthread.start()
    threads.append(newthread)
    clients = clients + 1
    print str(clients)
    

for t in threads:
    t.join()
    
    



