// IF !SUBJECT! !CONDITION! THEN !OBJECT! !ACTION!
var subject;
var condition=false;
var object;
var action=false;
var directMap=false;
var resultArray=[];


function setSubject(subj){
subject=subj;
}

function setCondition(cond){
condition=cond;
}

function setObject(obj){
	object=obj;
}

function setAction(act){
	action=act;
}
function doDirectMap(val){
	directMap=val;
}



function getState(){
var result="error";
if(!directMap){
result=["IF",subject,condition,"THEN",object,action];
}else{
result=["MAP",subject,condition,"TO",object,action];	
}
return result;
}

function getVerboseState(){
var result="error";
if(!directMap){
Tempresult=["IF",subject,condition,"THEN",object,action];
if(condition!==false){
switch(Tempresult[2][0]){
	case "GyroM":
	result=["IF",subject,"Gyro between "+condition[1]+" and "+condition[2],"THEN",object,action];
	break;
	case "GyroS":
	result=["IF",subject,"Gyro bigger than "+condition[1],"THEN",object,action];
	break;
	case "CapazM":
	result=["IF",subject,"Capacity between "+condition[1]+" and "+condition[2],"THEN",object,action];
	break;
	case "CapazS":
	result=["IF",subject,"Capacity bigger than "+condition[1],"THEN",object,action];
	break;
	case "P":
	result=["IF",subject,"Pressure is "+condition[1],"THEN",object,action];
	break;
}
if(action!==false){
switch(Tempresult[5][0]){
	case "L1":
	result[5]="Lights always on with color: "+action[1];
	break;
	case "L2":
	result[5]="Lights pulsating with color: "+action[1];
	break;
	case "L3":
	result[5]="Lights blinking with color: "+action[1];
	break;
	case "V":
	result[5]="Vibration with intensity: "+action[1];
	break;
	}
}else{
	alert("No actions set");
	return null;	
}
}else{
	alert("No conditions set");
	return null;
}
}else{
Tempresult=["MAP",subject,condition,"TO",object,action];
result=Tempresult;
if(condition!==false){
switch(Tempresult[2]){
	case "CapazMAP":
	result[2]="Capacity";
	break;
    case "GyroMAP":
	result[2]="Gyroscope";
	break;
}
}else{
	alert("No actions set");
	return null;
}
if(action!=false){
switch(Tempresult[5][0]){
	case "L1":
	result[5]="Lights always on with color: "+action[1];
	break;
	case "L2":
	result[5]="Lights pulsating with color: "+action[1];
	break;
	case "L3":
	result[5]="Lights blinking with color: "+action[1];
	break;
	case "V":
	result[5]="Vibration with intensity: "+action[1];
	break;
	}
}else{
	alert("No conditions set");
	return null;	
}
}
resultArray.push(result);
return result;
}


