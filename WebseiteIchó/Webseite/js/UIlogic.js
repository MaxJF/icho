var spheresL = [];
var spheresR = [];
var lines = [];
var Count = 0;
var readyAndConnected = false;
var firstListEnabled = false;
var secondListEnabled = false;
var LightState;
var color = "#ff0000";
var highestCount=0;
httpGetAsyncSpheres("http://127.0.0.1:8080/SphereCount?");


function SphereCountReceived(spheres) {
  console.log("starting");
  Count = spheres.substring(19, spheres.length);
  console.log(spheres);
  initUI();
}

function httpGetAsyncSpheres(theUrl, callback) {
  console.log("starting");
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
      SphereCountReceived(xmlHttp.responseText);
  }
  xmlHttp.open("GET", theUrl, true); // true for asynchronous 
  xmlHttp.send(null);
}

function httpGetAsync(theUrl, callback) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", theUrl, true); // true for asynchronous
  xmlHttp.send(null);
}


function initUI() {


  readyAndConnected = true;
  $('#directMapCapaz').val($(this).is(':checked'));
  var sphereLists = document.getElementsByClassName("spheres");
  for (var i = 0; i < sphereLists.length; i++) {
    sphereLists[i].innerHTML = '';


  }
  getConfig();

  for (var i = 0; i < sphereLists.length; i++) {
    for (var j = 0; j < Count; j++) {
      if (i === 0) {
        sphereLists[i].innerHTML += '<a href="#" id="kugel' + j + 'L' + '"class="list-group-item">Kugel' + (j + 1) + '</a>';
        spheresL.push("kugel" + j + "L");
      } else if (i == 1) {
        sphereLists[i].innerHTML += '<a href="#" id="kugel' + j + 'R' + '"class="list-group-item">Kugel' + (j + 1) + '</a>';
        spheresR.push("kugel" + j + "R");
      }
    }
  }
  for (var i = 0; i < spheresL.length; i++) {
    $("#" + spheresL[i]).click(function() {
      firstListEnabled = true;
      $("#listGyro").removeClass("disabled");
      $("#listCapaz").removeClass("disabled");
      $("#listPress").removeClass("disabled");
      setSubject(this.id);
      for (var j = 0; j < spheresL.length; j++) {
        $("#" + spheresL[j]).removeClass("active");
      }
      $(this).addClass("active");
    });
  }
  for (var i = 0; i < spheresR.length; i++) {
    $("#" + spheresR[i]).click(function() {
      secondListEnabled = true;
      $("#vibrationList").removeClass("disabled");
      $("#lightList").removeClass("disabled");
      setObject(this.id);
      for (var j = 0; j < spheresR.length; j++) {
        $("#" + spheresR[j]).removeClass("active");
      }
      $(this).addClass("active");
    });
  }


}

function getConfig() {
  document.getElementById("resultsList").innerHTML ="";
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
      var array = JSON.parse(xmlHttp.responseText);
    if (array) {

      for (var i = 0; i < array.length; i++) {
        //lines[i]='<div id='+i+'><a href="#" "class="list-group-item" style="width:80%;float:left;">'+array[i]+'</a><a id="'+i+'Click" style="float:left">X</a></div>';
        document.getElementById("resultsList").innerHTML += '<div id=' + i + '><a  "class="list-group-item" style="width:80%;float:left;">' + array[i] + '</a><a id="' + i + 'Click" style="float:left">X</a></div>';
        highestCount = i;

      };
      for(var i=0;i<array.length;i++){
        $("#"+i+"Click").click(function(){
           
          console.log(this.id[0]);
          httpGetAsync("http://127.0.0.1:8080/deleteLine?lineNR=" + i);
        });
      }

    }
  }
  xmlHttp.open("GET", 'http://127.0.0.1:8080/config', true);
  xmlHttp.send(null);
}
//GYRO 
$("#listGyro").click(function() {
  hideParas();
  if (firstListEnabled === true) {
    document.getElementById("paraGyro").style.display = "block";
    $("#listGyro").addClass("active");
    $("#listCapaz").removeClass("active");
    $("#listPress").removeClass("active");
  }

});

$("#gyroDirectMapping").click(function() {

  // $(".btn:first-child").text($(this).text());
  // $(".btn:first-child").val($(this).text());
  document.getElementById("GyroParaMultiTreshold").style.display = "none";
  document.getElementById("GyroParaSingleTreshold").style.display = "none";
  document.getElementById("GyroParaDirectMapping").style.display = "block";
});
$('input#directMap1').change(function() {
  if ($('input#directMap1').is(':checked')) {
    $('input#directMap1').addClass('checked');
    doDirectMap(true);
    setCondition("GyroMAP");
  } else {
    $('input#directMap1').removeClass('checked');
  }
});
$("#gyroSingleTresChoice").click(function() {

  //$(".btn:first-child").text($(this).text());
  //$(".btn:first-child").val($(this).text());
  doDirectMap(false);
  setCondition(["GyroS", 300]);
  document.getElementById("GyroParaMultiTreshold").style.display = "none";
  document.getElementById("GyroParaSingleTreshold").style.display = "block";
  document.getElementById("GyroParaDirectMapping").style.display = "none";
});
$("#gyroMultiTresChoice").click(function() {

  //$(".btn:first-child").text($(this).text());
  //$(".btn:first-child").val($(this).text());
  doDirectMap(false);
  setCondition(["GyroM", 75, 200]);
  document.getElementById("GyroParaMultiTreshold").style.display = "block";
  document.getElementById("GyroParaSingleTreshold").style.display = "none";
  document.getElementById("GyroParaDirectMapping").style.display = "none";
});

// CAPAZ
$("#listCapaz").click(function() {
  hideParas();
  if (firstListEnabled === true) {
    document.getElementById("paraCapaz").style.display = "block";
    $("#listGyro").removeClass("active");
    $("#listCapaz").addClass("active");
    $("#listPress").removeClass("active");
  }
});
$("#capazDirectMapping").click(function() {

  //$(".btn:first-child").text($(this).text());
  //$(".btn:first-child").val($(this).text());
  document.getElementById("CapazParaMultiTreshold").style.display = "none";
  document.getElementById("CapazParaSingleTreshold").style.display = "none";
  document.getElementById("CapazParaDirectMapping").style.display = "block";
});
$('input#directMapCapaz').change(function() {
  if ($('input#directMapCapaz').is(':checked')) {
    $('input#directMapCapaz').addClass('checked');
    doDirectMap(true);
    setCondition("CapazMAP");
  } else {
    $('input#directMapCapaz').removeClass('checked');
  }
});
$("#capazSingleTresChoice").click(function() {

  //$(".btn:first-child").text($(this).text());
  //$(".btn:first-child").val($(this).text());
  doDirectMap(false);
  setCondition(["CapazS", 300]);
  document.getElementById("CapazParaMultiTreshold").style.display = "none";
  document.getElementById("CapazParaSingleTreshold").style.display = "block";
  document.getElementById("CapazParaDirectMapping").style.display = "none";
});
$("#capazMultiTresChoice").click(function() {

  //$(".btn:first-child").text($(this).text());
  //$(".btn:first-child").val($(this).text());
  doDirectMap(false);
  setCondition(["CapazM", 75, 350]);
  document.getElementById("CapazParaMultiTreshold").style.display = "block";
  document.getElementById("CapazParaSingleTreshold").style.display = "none";
  document.getElementById("CapazParaDirectMapping").style.display = "none";
});
// PRESSURE
$("#listPress").click(function() {
  hideParas();
  if (firstListEnabled === true) {
    doDirectMap(false);
    setCondition(["P", true]);
    document.getElementById("paraPress").style.display = "block";
    $("#listGyro").removeClass("active");
    $("#listCapaz").removeClass("active");
    $("#listPress").addClass("active");
  }

  $('#paraPressToggle').change(function() {
    doDirectMap(false);
    setCondition(["P", $(this).prop('checked')]);
  });
});

function hideParas() {
  document.getElementById("paraCapaz").style.display = "none";
  document.getElementById("paraGyro").style.display = "none";
  document.getElementById("paraPress").style.display = "none";
}


//LIGHTS
$("#lightList").click(function() {
  hideActorsParas();
  if (secondListEnabled === true) {
    document.getElementById("paraLights").style.display = "block";
    document.getElementById("LightsColor").style.display = "block";
    $("#lightList").addClass("active");
    $("#vibrationList").removeClass("active");
  }
});
$('input#lights1').change(function() {
  if ($('input#lights1').is(':checked')) {
    LightState = "L1";
    setAction([LightState, color]);
    $('input#lights1').addClass('checked');
    $('input#lights2').removeClass('checked');
    $('input#lights3').removeClass('checked');
    $('input#lights2').prop('checked', false);
    $('input#lights3').prop('checked', false);
  } else {
    $('input#lights1').removeClass('checked');
  }
});
$('input#lights2').change(function() {
  if ($('input#lights2').is(':checked')) {
    LightState = "L2";
    setAction([LightState, color]);
    $('input#lights2').addClass('checked');
    $('input#lights1').removeClass('checked');
    $('input#lights3').removeClass('checked');
    $('input#lights1').prop('checked', false);
    $('input#lights3').prop('checked', false);
  } else {
    $('input#lights2').removeClass('checked');
  }
});
$('input#lights3').change(function() {
  if ($('input#lights3').is(':checked')) {
    LightState = "L3";
    setAction([LightState, color]);
    $('input#lights3').addClass('checked');
    $('input#lights1').removeClass('checked');
    $('input#lights2').removeClass('checked');
    $('input#lights1').prop('checked', false);
    $('input#lights2').prop('checked', false);
  } else {
    $('input#lights3').removeClass('checked');
  }
});

$("#colorInput").change(function() {
  color = this.value;
});

// Vibration
$("#vibrationList").click(function() {
  hideActorsParas();
  if (secondListEnabled === true) {
    setAction(["V", 300]);
    document.getElementById("paraVibration").style.display = "block";
    $("#vibrationList").addClass("active");
    $("#lightList").removeClass("active");
  }
});
$("#save").click(function() {
  returnString = getVerboseState().toString();
  returnString = returnString.replace(/,/g, ' - ');
  highestCount++;
  document.getElementById("resultsList").innerHTML += '<div id=' + highestCount + '><a href="#" "class="list-group-item" style="width:80%;float:left;">' + returnString + '</a><a id="' + highestCount + 'Click" style="float:left">X</a></div>';
  $("#" + highestCount + "Click").click(function() {
    document.getElementById("resultsList").innerHTML = "";
    getConfig();
  });
  httpGetAsync("http://127.0.0.1:8080/writeLine?line=" + getState());
});


$("#reset").click(function() {
  $("#listGyro").removeClass("active");
  $("#listCapaz").removeClass("active");
  $("#listPress").removeClass("active");
  $("#vibrationList").removeClass("active");
  $("#lightList").removeClass("active");
 

  for (i = 0; i < spheresR.length; i++) {
    $("#" + spheresR[i]).removeClass("active");
  }
  for (i = 0; i < spheresL.length; i++) {
    $("#" + spheresL[i]).removeClass("active");
    
  }
  hideParas();
  hideActorsParas();

});
$("#run").click(function() {
  httpGetAsync("http://127.0.0.1:8080/run");
});


function hideActorsParas() {
  document.getElementById("LightsColor").style.display = "none";
  document.getElementById("paraLights").style.display = "none";
  document.getElementById("paraVibration").style.display = "none";
}