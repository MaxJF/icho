$(function() {

    $( "#GyroMultslider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75,200],
      slide: function( event, ui ) {
        $( "#GyroMultipleTreshAmount" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
        doDirectMap(false);
        setCondition(["GyroM",ui.values[0],ui.values[1]]);
      }
    });

    $( "#GyroMultipleTreshAmount" ).val( "" + $( "#GyroMultslider-range" ).slider( "values", 0 ) +
      " - " + $( "#GyroMultslider-range" ).slider( "values", 1 ) );
  });


$(function() {

    $( "#GyroSingleslider" ).slider({
      range: false,
      min: 0,
      max: 500,
      values: [ 300 ],
      slide: function( event, ui ) {
        $( "#GyroSingleTreshAmount" ).val( "" + ui.values[ 0 ]);
        doDirectMap(false);
        setCondition(["GyroS",ui.values[0]]);
      }
    });
    $( "#GyroSingleTreshAmount" ).val( "" + $( "#GyroSingleslider" ).slider( "values", 0 ) 
      );
  });


$(function() {

    $( "#CapazMultslider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 350 ],
      slide: function( event, ui ) {
        $( "#CapazMultipleTreshAmount" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
        doDirectMap(false);
        setCondition(["CapazM",ui.values[0],ui.values[1]]);
      }
    });
    $( "#CapazMultipleTreshAmount" ).val( "" + $( "#CapazMultslider-range" ).slider( "values", 0 ) +
      " - " + $( "#CapazMultslider-range" ).slider( "values", 1 ) );
  });
$(function() {

    $( "#CapazSingleslider" ).slider({
      range: false,
      min: 0,
      max: 500,
      values: [ 300 ],
      slide: function( event, ui ) {
        $( "#CapazSingleTreshAmount" ).val( "" + ui.values[ 0 ]);
        doDirectMap(false);
        setCondition(["CapazS",ui.values[0]]);
      }
    });
    $( "#CapazSingleTreshAmount" ).val( "" + $( "#CapazSingleslider" ).slider( "values", 0 ) );
  });
$(function() {

    $( "#VibrationSingleslider" ).slider({
      range: false,
      min: 0,
      max: 500,
      values: [ 300 ],
      slide: function( event, ui ) {
        $( "#VibrationSingleTreshAmount" ).val( "" + ui.values[ 0 ]);
        setAction(["V",ui.values[0]]);

      }
    });

  });
