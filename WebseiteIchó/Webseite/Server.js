/*---- DEBUGGING VARIABLES ----*/
var debugInfoFromSpheres = true;
var debugOutgoingInfo = false;
/*---- DEBUGGING VARIABLES END ----*/
var pressureTresholdActivation = 200;
var http = require('http');
var fs = require('fs');
var lineReader = require('line-reader');
var path = require('path');
var net = require('net');
var server = net.createServer();
var connect = require('connect');
var sendStrings = [
  []
];
var connections = [];
var clients = -1;
var SphereOutsputs = [];
var GyroOutputK1 = "";
var GyroOutputK2 = "";
var resGlob;
var linesReadFromFile = [];


/*---- WEBSERVER START----*/
var express = require('express'),
  app = express();
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use(express.static('js'));
app.use(express.static('css'));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded());

app.get('/', function(req, res) {
  res.render(__dirname + '/index.html', {
    k1Gyro: GyroOutputK1,
    k2Gyro: GyroOutputK2
  });

});
app.get('/SphereCount', function(req, res) {
  res.send("spheres connected: " + connections.length);

});
app.get('/config', function(req, res) {
  fs.readFile('config', function(err, data) {
    if (err) throw err;
    var array = data.toString().replace('\r', '').split("\n");
    array.splice(array.length-1, 1);
    for (i in array) {
      array[i] = array[i].replace('\r', '');
    }
    res.send(array);

  });


});
app.get('/writeLine', function(req, res) {
  console.log(req.query.line);
  saveDataToFile(req.query.line);
  res.send(null);
});
app.get('/deleteLine',function(req,res){
  console.log(req.query.lineNR);
  res.send(null);
})


app.get('/run', function(req, res) {
  var lineTemp = [];
  fs.readFile('config', function(err, data) {
    if (err) throw err;
    var array = data.toString().replace('\r', '').split("\n");
    array.splice(0, 1);
    for (i in array) {
      array[i] = array[i].replace('\r', '');
    }
    setVar(array);

  });
  res.send(null);

});
console.log("Webserver listening on port 8080 - Connect via http://ip:8080 - or http://localhost:8080");
app.listen(8080);

function setVar(lines) {
  linesReadFromFile = lines;

}

function sendDataToSphere(kugel, type, intensity, color, lightsType, intervall) {

}

function sendDataToSphereOLD(kugel, color) {


  if (sendStrings[kugel - 1]) {
    switch (color) {
      case 'R':
        sendStrings
          [kugel - 1] = ("0,254,0,0,0");
        console.log("R");
        break;
      case 'G':
        sendStrings
          [kugel - 1] = ("0,0,254,0,0");
        console.log("G");
        break;
      case 'B':
        sendStrings
          [kugel - 1] = ("0,0,0,254,0");
        console.log("B");
        break;
    }
    console.log("setting Sending Array for sphere " + kugel + " to " + sendStrings[kugel - 1]);
    console.log("now there are " + sendStrings.length + "Strings for " + connections.length + "spheres registered");
  }


}

function saveDataToFile(line) {

  fs.stat('config', function(err, stat) {
    if (err == null) {
      console.log('File exists');
      fs.appendFile('config', line + "\r\n", function(err) {
        console.log('Some other error: ', err);
      });
    } else if (err.code == 'ENOENT') {
      // file does not exist
      fs.writeFile('config', line + "\r\n");
    } else {
      console.log('Some other error: ', err);
    }
  });
}


/*---- WEBSERVER END ----*/


/*---- KUGEL HANDLING START ----*/
server.on('connection', handleConnection);

server.listen(8000, function() {
  //console.log('server listening to %j', server.address());
});

function handleConnection(conn) {
  var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
  connections.push(conn);
  sendStrings.push("0,0,0,0,0");
  //SphereOutsputs.push("0");
  conn.sphereNumber = connections.length;
  console.log('new client connection from %s', remoteAddress);

  var thisclient = clients;

  conn.on('data', onConnData);
  conn.once('close', onConnClose);
  conn.on('error', onConnError);

  function onConnData(d) {

    SphereOutsputs[conn.sphereNumber - 1] = d.toString().split(',');
    sendingLogic();
    if (debugInfoFromSpheres) {
      console.log('connection data from %s: %j', remoteAddress, SphereOutsputs[conn.sphereNumber-1].toString());
    }

    if (sendStrings[conn.sphereNumber - 1]) {
      conn.write(sendStrings[conn.sphereNumber - 1].toString().replace('[', '').replace(']', ''));
      if (debugOutgoingInfo) {
        console.log("Data send to Sphere #" + conn.sphereNumber + ": " + sendStrings[conn.sphereNumber - 1])
      }

    } else {
      conn.write("0,0,0,0,0");
      if (debugOutgoingInfo) {
        console.log("Data send to Sphere #" + conn.sphereNumber + ": 0,0,0,0,0")
      }
    }

  }

  function onConnClose() {

    console.log('connection from %s closed', remoteAddress);
    console.log("DELETING SPHERE #" + conn.sphereNumber);
    if (conn.sphereNumber < connections.length) {
      for (var i = 0; i < connections.length; i++) {
        connections[i].sphereNumber--;
      };
    }
    connections.splice(conn.sphereNumber - 1, 1);
    sendStrings.splice(conn.sphereNumber - 1, 1);

  }

  function onConnError(err) {
    console.log('Connection %s error: %s', remoteAddress, err.message);
  }
}

function sendingLogic() {

  for (var i = 0; i < linesReadFromFile.length; i++) {

    if (linesReadFromFile[i][0] == 'I') {
      var array = linesReadFromFile[i].toString().split(',');
      //console.log(array.toString());


      var sensorSphere = array[1];
      sensorSphere = sensorSphere.match(/\d/g);
      sensorSphere = sensorSphere.join("");
      var actorSphere = array[5];
      actorSphere = actorSphere.match(/\d/g);
      actorSphere = actorSphere.join("");



      switch (array[2]) {
        case 'P':
          switch (array[3]) {
            case 'true':
              sendStrings[actorSphere - 1][0] = 255;
              //console.log(SphereOutsputs[0].toString());
              break;
            case 'false':
              sendStrings[actorSphere - 1][0] = 0;
              break;
          }
          break;
        case 'GyroM':
          break;
        case 'GyroS':
          break;
        case 'CapazM':
          break;
        case 'CapazS':
          break;
      }

    } else if (linesReadFromFile[i][0] == 'M') {
      var array = linesReadFromFile[i].toString().split(',');
    }
  }
}

function convertIncommingString(num) {
  var array = SphereOutsputs[num].toString().split(',');
  console.log(array[2]);

}

function convertASCII(num) {
  return String.fromCharCode(num);
}
/*---- KUGEL HANDLING END ----*/
