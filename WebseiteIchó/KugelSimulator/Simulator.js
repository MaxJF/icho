var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var http = require('http');
var clients = [];

app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use(express.static('js'));
app.use(express.static('css'));
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded());	
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res) {
  res.render(__dirname + '/index.html');
});

app.get('/simulateSpheres', function(req,res){
for(var i=0;i<req.query.val;i++){
	addClient();
}
	res.send(null);
});



app.get('/valueChanged',function(req,res){
	
clients[req.query.sphere-1].send({msgtype:req.query.sensor,content:req.query.val});
res.send(null);
})



app.listen(3000, function () {
  console.log('Connect via http://ip:3000 - OR - http://localhost:3000');
});


function addClient(){

var exec = require('child_process').fork('ChildClient.js');
clients.push(exec);
}

