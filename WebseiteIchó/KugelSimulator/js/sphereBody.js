function getBody(num){
$("#spheresBody").append('<div class="kugel" ><div class="col-md-3"><div class="panel panel-default"><div class="panel-body"><h4> Kugel '+num+'</h4><p>Pressure</p><div class="col-md-10"><input id="P'+num+'"class="slider"  type="range" min="0" max="255" step="1" /></div><div class="col-md-1"><p id="PV'+num+'"class="sliderVal">128</p></div><p>Gyro</p><div class="col-md-10"><input class="slider"  id="GX'+num+'" type="range" min="0" max="255" step="1" /></div><div class="col-md-1"><p id="GXV'+num+'"class="sliderVal">128</p></div><div class="col-md-10"><input class="slider"  id="GY'+num+'"type="range" min="0" max="255" step="1" /></div><div class="col-md-1"><p id="GYV'+num+'" class="sliderVal">128</p></div><div class="col-md-10"><input class="slider"  id="GZ'+num+'"type="range" min="0" max="255" step="1" /></div><div class="col-md-1"><p id="GZV'+num+'" class="sliderVal">128</p></div><p>Acceleration</p><div class="col-md-10"><input class="slider"  id="AX'+num+'"type="range" min="0" max="255" step="1" /></div><div class="col-md-1"><p id="AXV'+num+'"class="sliderVal">128</p></div><div class="col-md-10"><input id="AY'+num+'" class="slider"  type="range" min="0" max="255" step="1" /></div><div class="col-md-1"><p id="AYV'+num+'" class="sliderVal">128</p></div><div class="col-md-10"><input id="AZ'+num+'" class="slider"  type="range" min="0" max="255" step="1" /></div><div class="col-md-1"><p id="AZV'+num+'" class="sliderVal">128</p></div><p>Capacity</p><div class="col-md-10"><input id="C'+num+'" class="slider"  type="range" min="0" max="255" step="1" /></div><div class="col-md-1"><p id="CV'+num+'" class="sliderVal">128</p></div></div></div></div></div>');
registerSliderListeners(num);
}


function registerSliderListeners(num){
$('#P'+num).on("change mousemove", function() {
    	 $('#PV'+num).html($(this).val());
    	 httpGetAsync("http://localhost:3000/valueChanged?val="+$(this).val()+"&sphere="+num+"&sensor=P");
});
$('#GX'+num).on("change mousemove", function() {
    	 $('#GXV'+num).html($(this).val());
    	 httpGetAsync("http://127.0.0.1:3000/valueChanged?val="+$(this).val()+"&sphere="+num+"&sensor=GX");
});
$('#GY'+num).on("change mousemove", function() {
    	 $('#GYV'+num).html($(this).val());
    	 httpGetAsync("http://127.0.0.1:3000/valueChanged?val="+$(this).val()+"&sphere="+num+"&sensor=GY");
});
$('#GZ'+num).on("change mousemove", function() {
    	 $('#GZV'+num).html($(this).val());
    	 httpGetAsync("http://127.0.0.1:3000/valueChanged?val="+$(this).val()+"&sphere="+num+"&sensor=GZ");
});
$('#AX'+num).on("change mousemove", function() {
    	 $('#AXV'+num).html($(this).val());
    	 httpGetAsync("http://127.0.0.1:3000/valueChanged?val="+$(this).val()+"&sphere="+num+"&sensor=AX");
});
$('#AY'+num).on("change mousemove", function() {
    	 $('#AYV'+num).html($(this).val());
    	 httpGetAsync("http://127.0.0.1:3000/valueChanged?val="+$(this).val()+"&sphere="+num+"&sensor=AY");
});
$('#AZ'+num).on("change mousemove", function() {
    	 $('#AZV'+num).html($(this).val());
    	 httpGetAsync("http://127.0.0.1:3000/valueChanged?val="+$(this).val()+"&sphere="+num+"&sensor=AZ");
});
$('#C'+num).on("change mousemove", function() {
    	 $('#CV'+num).html($(this).val());
    	 httpGetAsync("http://127.0.0.1:3000/valueChanged?val="+$(this).val()+"&sphere="+num+"&sensor=C");
});
$(".kugel").delay(100).animate({"opacity": "1"}, 700);
}

